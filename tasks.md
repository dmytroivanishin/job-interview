# Задача 1

Реализовать функцию filter, по аналогии метода для массива.
Результат функции, отфильтрованный массив по возрасту.

```
const ages = [18, 5, 8, 9, 44, 26, 15, 3, 21, 17];

function filter() {
  // Реализация
}

filter(
  ages,
  age => age >= 18
); // [18, 44, 26, 21]
```

# Задача 2

Реализовать функцию reduce, по аналогии метода для массива.
Результат функции, сумма всех чисел из массива.
При этом, вызов первой функции должен вернуть 15, вызов второй функции 20.

```
const numbers = [1, 2, 3, 4, 5];

function reduce() {
  // Реализация
}

reduce(
  numbers,
  (prevNumber, curNumber) => prevNumber + curNumber
); // 15

reduce(
  numbers,
  (prevNumber, curNumber) => prevNumber + curNumber,
  5
); // 20
```

# Задача 3

Реализовать функцию с помощью метода массива reduce, которая получает максимальное число из неупорядоченного массива.
```
const numbers = [2, 5, 4, 10, 8, 1, 7, 6, 3];

function getMax() {
  // Реализация
}

getMax(numbers); // 10
```

# Задача 4

Реализовать функцию, которая на вход принимает объект.
В качестве ключей объекта – это буквы, а значение, - это позиции, на которых должны стоять буквы.
Результат функции строка "Hello World!"

```
const HelloWorld = {
  o: [4, 7],
  e: [1],
  l: [2, 3, 9],
  r: [8],
  "!": [11],
  d: [10],
  W: [6],
  " ": [5],
  H: [0],
};

function sayHi(data) {
  // Реализация
}

sayHi(HelloWorld); // Hello World!
```

# Задача 5

Реализовать функцию compose, которая принимает на вход как аргументы любое количество функций. 
Результат этой функции,- функция обратного вызова, при вызове которой получаем разультат.

```
const add = n => n + 1;
const multiply = n => n * 2;

const compose = () => {
  // Реализация
}

const combinedFunc = compose(add, multiply);

combinedFunc(1); // 4
```

# Задача 6

Реализовать асинхронную функцию compose, которая принимает на вход как аргументы любое количество асинхронных функций. 
Результат этой функции, - функция обратного вызова, при вызове которой получаем результат.

```
const add = n => Promise.resolve(n + 1);
const multiply = n => Promise.resolve(n * 2);

const compose = () => {
  // Реализация
}

const combinedFunc = compose(add, multiply);

combinedFunc(1).then(console.log); // 4
```

# Задача 7

Реализовать функцию flat, которая многомерный, вложенный массив превращает в одномерный.
Результат функции, - одномерный массив от 1 до 10.

```
const numbers = [1, 2, [3, 4], [5, [6, 7], 8], 9, 10];

const flat = (data) => {
  // Реализация
}

flat(numbers); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

# Задача 8

Реализовать функцию promisify, которая принимает на вход любые функции, которые работают на функциях обратного вызова.
Иными словами сделать промисификацию. 
Каждая функция может принимать на вход любое количесво необходимых аргументов.

```
const promisify = () => {
  // Реализация
}

// Обычная работа функции
readFile('./documentation.txt', { encoding: 'utf-8' }, (error, data) => {...});

// После промисификации
const readFilePromise = promisify(readFile);

readFilePromise('./documentation.txt', { encoding: 'utf-8' })
  .then(console.log)
  .catch(console.log);

// Обычная работа функции
writeFile('./documentation.txt', (error, data) => {...});

// После промисификации
const writeFilePromise = promisify(writeFile);

writeFilePromise('./documentation.txt')
  .then(console.log)
  .catch(console.log);
```

# Задача 9

Реализовать функцию debounce.
Результат вызова функции будет функция, которая передается обработчику событий input, который будет ожидать пока пользователь введет данные и сработает 200 мс, вместо того, чтобы срабатывать при каждом нажатии клавиш.

```
const debounce = () => {
  // Реализация
};

const handleInput = (e) => console.log(e.target.value);
const handleDebounce = debounce(handleInput , 200);

const input = document.querySelector("input");

input.addEventListener("input", handleDebounce);
```

# Задача 10

Реализовать небольшой собственный шаблонизатор. Передается HTML как строка с токенами. Необходимо сделать подстановки на реальные значения. Важно! Чтобы все было динамически и не было привязок к конкретным названиям, и чтобы можно было изменить токены на HTML, передать необходимые аргументы и все работало.

```
const transform = () => {
  // Реализация
}

const template = `
  <html lang="en">
    <head>
      <title>{{ title }}</title>
    </head>
    <body>
      <h1>{{ title }}</h1>
      <img src="{{ src }}"/>
      <p>{{ description }}</p>
    </body>
  </html>
`;

transform(
  template,
  {
    title: 'JavaScript',
    description: 'It is a lightweight interpreted programming language',
    src: 'https://javascript.jpg'
  }
)

/*
** Результат:

<html lang="en">
  <head>
    <title>JavaScript</title>
  </head>
  <body>
    <h1>JavaScript</h1>
    <img src="https://javascript.jpg"/>
    <p>It is a lightweight interpreted programming language</p>
  </body>
</html>
*/
```

# Задача 11

Реализовать TypeScript утилиту Exclude.
Результат работы утилиты 'read' | 'create'.

```
type Exclude = // Реализация

type Actions = 'read' | 'create' | 'update' | 'delete';

type ExcludedTypes = Exclude<Actions, 'update' | 'delete'>;

// 'read' | 'create'
```

# Задача 12

Запустить контейнер Docker с базой данных из образа mongo с версией 3.6.
При этом, чтобы была возможность подключиться локально к этой базе данных и работать с ней.

```
// Написать команду:

> 
```

# Задача 13

Создать образ и запустить свой контейнер из созданного образа.

```
// Написать команды:

> 

> 
```

# Задача 14

Оптимизация Docker образа. 
Почему сначала копируют файлы COPY package*.json ./, потом устанавливают зависимоти и только потом копируют остальные файлы проекта ?

```
FROM node:latest

WORKDIR .

COPY ./package*.json .

RUN npm install

COPY . .

RUN npm run build
```

# Задача 15

Устно. Необходимо реализовать пагинацию lazy loading. Каждый новый запрос должен получать по 10 новых записей.

Для оптимизации производительности работы сервера есть ограничения:
- запрещено делать запросы на count всех записей
- запрещено делать дополнительные запросы на определение "есть ли следущая порция" записей

Так же сервер не отдает, "есть ли следущая порция" записей.

Все что есть, это параметры skip и limit, которые могут участвовать в запросах, и сервер, который отдает то количество данных, которое запросили.

Каким образом определять что "есть следущая порция данных" и как построить запросы, чтобы получить необходимый результат, с правильно работающей пагинайцией и с условиями описанными выше и не делать запросы на севрер, когда данные закончились ?

P.S. Количество записей мы не знаем, их может быть 99, может 100 или 456 или любое другое количество.