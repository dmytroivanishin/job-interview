## Job Interview
* [Questions](https://gitlab.com/dmytroivanishin/job-interview/-/blob/master/questions.md)
* [Tasks](https://gitlab.com/dmytroivanishin/job-interview/-/blob/master/tasks.md)

Useful [article](https://habr.com/ru/company/kts/blog/583816/) about job interview and its structure.

Online Editors for job interview for test tasks:
* [Replit](https://replit.com/)
* [Codefile](https://codefile.io/)
* [Collabedit](https://collabedit.com/)
* [Codeshare](https://codeshare.io/)