# Завдання 1

Реалізувати функцію filter, по аналогії метода для масива.
Результат функції, відфільтрований масив по віку.

```
const ages = [18, 5, 8, 9, 44, 26, 15, 3, 21, 17];

function filter() {
  // Реалізація
}

filter(
  ages,
  age => age >= 18
); // [18, 44, 26, 21]
```

# Завдання 2

Реалізувати функцію reduce, по аналогії метода для масива.
Результат функції, сума усії чисел з масива.
При цьому, виклик першої функції має повернути 15, виклик другої функції 20.

```
const numbers = [1, 2, 3, 4, 5];

function reduce() {
  // Реалізація
}

reduce(
  numbers,
  (prevNumber, curNumber) => prevNumber + curNumber
); // 15

reduce(
  numbers,
  (prevNumber, curNumber) => prevNumber + curNumber,
  5
); // 20
```

# Завдання 3

Реалізувати функцію за допомогою метода масива reduce, яка отримує максимальне число з неупорядкованного масива.
```
const numbers = [2, 5, 4, 10, 8, 1, 7, 6, 3];

function getMax() {
  // Реалізація
}

getMax(numbers); // 10
```

# Завдання 4

Реалізувати функцію, яка на вхід приймає об'єкт.
В якості ключей об'єкта – це літери, а значення, - це позиції, на яких мають стояти літери.
Результат функції рядок "Hello World!"

```
const HelloWorld = {
  o: [4, 7],
  e: [1],
  l: [2, 3, 9],
  r: [8],
  "!": [11],
  d: [10],
  W: [6],
  " ": [5],
  H: [0],
};

function sayHi(data) {
  // Реалізація
}

sayHi(HelloWorld); // Hello World!
```

# Завдання 5

Реалізувати функцію compose, яка принимає на вхід як аргументи будь яку кількість функцій. 
Результат цієї функції,- функція зворотьного виклика, під час виклику якої отримаємо результат.

```
const add = n => n + 1;
const multiply = n => n * 2;

const compose = () => {
  // Реалізація
}

const combinedFunc = compose(add, multiply);

combinedFunc(1); // 4
```

# Завдання 6

Реалізувати функцію compose, яка принимає на вхід як аргументи будь яку кількість асінхроних функцій. 
Результат цієї функції,- функція зворотьного виклика, під час виклику якої отримаємо результат.

```
const add = n => Promise.resolve(n + 1);
const multiply = n => Promise.resolve(n * 2);

const compose = () => {
  // Реалізація
}

const combinedFunc = compose(add, multiply);

combinedFunc(1).then(console.log); // 4
```

# Завдання 7

Реалізувати функцію flat, яка многомірний, вкладений масив перетворює в одномірний.
Результат функції, - одномірний масив від 1 до 10.

```
const numbers = [1, 2, [3, 4], [5, [6, 7], 8], 9, 10];

const flat = (data) => {
  // Реалізація
}

flat(numbers); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

# Завдання 8

Реалізувати функцію promisify, яка приймає на вхід будь які функції, які працюють за допомогою функцій зворотьного виклику.
Іншими словами зобити промісіфікацію. 
Кожна функція може приймати на вхід будь яку кількість потрібних аргументів.

```
const promisify = () => {
  // Реалізація
}

// Звичайна робота функції
readFile('./documentation.txt', { encoding: 'utf-8' }, (error, data) => {...});

// Після промісіфікації
const readFilePromise = promisify(readFile);

readFilePromise('./documentation.txt', { encoding: 'utf-8' })
  .then(console.log)
  .catch(console.log);

// Звичайна робота функції
writeFile('./documentation.txt', (error, data) => {...});

// Після промісіфікації
const writeFilePromise = promisify(writeFile);

writeFilePromise('./documentation.txt')
  .then(console.log)
  .catch(console.log);
```

# Завдання 9

Реалізувати функцію debounce.
Результат виклика функції буде функція, яка передається обробнику подій input, який буде очікувати доки користувач введе дані і спрацює 200 мс, замість того, щоб спрацьовувати при кожному натисканню клавіш.

```
const debounce = () => {
  // Реалізація
};

const handleInput = (e) => console.log(e.target.value);
const handleDebounce = debounce(handleInput , 200);

const input = document.querySelector("input");

input.addEventListener("input", handleDebounce);
```

# Завдання 10

Реалізувати невеликий власний шаблонізатор. Передається HTML як строка з токенами. Треба зробити підстановлення на реальні значення. Важливо! Щоб воно було динамічне і не було прив'язане до конкретних назв, щоб можна було змінити токен на HTML, передати потрібні аргументи і все працювало.

```
const transform = () => {
  // Реалізація
}

const template = `
  <html lang="en">
    <head>
      <title>{{ title }}</title>
    </head>
    <body>
      <h1>{{ title }}</h1>
      <img src="{{ src }}"/>
      <p>{{ description }}</p>
    </body>
  </html>
`;

transform(
  template,
  {
    title: 'JavaScript',
    description: 'It is a lightweight interpreted programming language',
    src: 'https://javascript.jpg'
  }
)

/*
** Результат має бути:

<html lang="en">
  <head>
    <title>JavaScript</title>
  </head>
  <body>
    <h1>JavaScript</h1>
    <img src="https://javascript.jpg"/>
    <p>It is a lightweight interpreted programming language</p>
  </body>
</html>
*/
```

# Завдання 11

Реалізувати TypeScript утіліту Exclude.
Результат роботы утіліти 'read' | 'create'.

```
type Exclude = // Реалізація

type Actions = 'read' | 'create' | 'update' | 'delete';

type ExcludedTypes = Exclude<Actions, 'update' | 'delete'>;

// 'read' | 'create'
```

# Завдання 12

Запустити контейнер Docker з базою даних з образа mongo та версії 3.6.
При цьому, щоб була можливість подключитися локально до цієї бази даних та працювати с нею.

```
// Написати команду:

> 
```

# Завдання 13

Створити образ і запустити свій контейнер із створеного образа.

```
// Написать команды:

> 

> 
```

# Завдання 14

Оптимізація Docker образа. 
Чому спочатку копіюють файли COPY package*.json ./, а потім встановлюють залежності і тільки потім копіюють інші файли проекта ?

```
FROM node:latest

WORKDIR .

COPY ./package*.json .

RUN npm install

COPY . .

RUN npm run build
```

# Завдання 15

Усно. Потрібно реалізувати пагінацію lazy loading. Кожен новий запит має отримувати по 10 нових записів.

Для оптимізации продуктивності работи сервера є обмеження:
- заборонено робити запити на count для усіх записів
- заборонено робити додаткові запити на визначення "а чи є наступна порція" записів

Токаж сервер не повертає, "що є наступна порция" записів.

Все що є, це параметри skip та limit, які можуть приймати участь у запитах, і сервер, який повертає ту кількість даних, які запросили.

Яким чином з'ясувати що "є наступна порция даних" і як побудувати запити, щоб отримати необхідний результат, з правильно працюючою пагінайцією і з вимогами які описані вище і не робити запити на севрер коли дані закінчились ?

P.S. Кількість записів ми не знаємо, їх може бути 99, може 100 або 456 або будь яка інша кількість.